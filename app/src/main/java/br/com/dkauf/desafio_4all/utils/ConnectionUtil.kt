package br.com.dkauf.desafio_4all.utils

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import androidx.lifecycle.LiveData

object ConnectionUtil : LiveData<Boolean>() {

    private lateinit var application: Application

    fun init(application: Application) {
        this.application = application
    }

    fun hasInternet(): Boolean {
        val connectivityManager = application.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return connectivityManager.activeNetworkInfo != null && connectivityManager.activeNetworkInfo.isConnected
    }
}