package br.com.dkauf.desafio_4all.model

data class TaskDetailsResponse(
    val id: String?,
    val cidade: String?,
    val bairro: String?,
    val urlFoto: String?,
    val urlLogo: String?,
    val titulo: String?,
    val telefone: String?,
    val texto: String?,
    val endereco: String?,
    val latitude: Double,
    val longitude: Double,
    val comentarios: List<CommentsResponse>?
){
    fun cityNeighborhoodFormatted(): String{
        return "${cidade} - ${bairro}"
    }
}

