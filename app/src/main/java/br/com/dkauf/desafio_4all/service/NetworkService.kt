package br.com.dkauf.desafio_4all.service

import br.com.dkauf.desafio_4all.model.TaskDetailsResponse
import br.com.dkauf.desafio_4all.model.TaskResponse
import io.reactivex.Single

class NetworkService constructor(private var networkApi: NetworkApi) {

    fun getTasks(): Single<TaskResponse> {
        return networkApi.getTasks()
    }

    fun getTasksDetails(id:String): Single<TaskDetailsResponse> {
        return networkApi.getTasksDetails(id)
    }
}