package br.com.dkauf.desafio_4all.di

import androidx.annotation.VisibleForTesting
import br.com.dkauf.desafio_4all.BuildConfig
import br.com.dkauf.desafio_4all.service.NetworkApi
import br.com.dkauf.desafio_4all.service.NetworkService
import br.com.dkauf.desafio_4all.viewmodel.MainViewModel
import br.com.dkauf.desafio_4all.viewmodel.PrincipalViewModel
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

fun loadCreditKoinModules() {
    loadKoinModules(
        listOf(
            viewModelModule,
            servicesModule
        )
    )
}

@VisibleForTesting
val viewModelModule = module {
    viewModel {
        MainViewModel(get(), get())
    }
    viewModel {
        PrincipalViewModel(get(), get())
    }
}

@VisibleForTesting
val servicesModule = module {
    factory { GsonBuilder().create() }

    single {
        createOkHttpClient()
    }

    single {
        Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(get()))
            .client(get())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    single {
        get<Retrofit>().create(NetworkApi::class.java)
    }

    single {
        NetworkService(get())
    }
}

fun createOkHttpClient(): OkHttpClient {
    HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY

        return OkHttpClient.Builder()
            .addInterceptor(this)
            .build()
    }
}