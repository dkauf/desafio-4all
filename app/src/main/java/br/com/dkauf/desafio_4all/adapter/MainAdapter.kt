package br.com.dkauf.desafio_4all.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import br.com.dkauf.desafio_4all.R
import br.com.dkauf.desafio_4all.databinding.ContentMainTextBinding
import br.com.dkauf.desafio_4all.ui.activity.PrincipalActivity

class MainAdapter(
    val context: Context?,
    var list: List<String>?
) : RecyclerView.Adapter<MainAdapter.ViewHolder>() {

    companion object {
        const val BUILD_EXTRA = "BUILD_EXTRA"
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.content_main_text,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list?.size ?: 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        list?.apply {
            holder.setData(this[position])
        }
    }

    class ViewHolder(val binding: ContentMainTextBinding) : RecyclerView.ViewHolder(binding.root) {
        fun setData(content: String) {
            binding.txtValue.apply{
                text = content
                setOnClickListener {
                    Intent(context, PrincipalActivity::class.java).apply {
                        putExtra(BUILD_EXTRA, content)
                        context.startActivity(this@apply)
                    }
                }
            }

        }
    }
}