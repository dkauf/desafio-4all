package br.com.dkauf.desafio_4all.utils

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

@BindingAdapter("profileImage")
fun loadImage(view: ImageView, imageUrl: String?) {
    imageUrl?.apply {
        Glide.with(view.context)
            .load(this)
            .into(view)
    }
}