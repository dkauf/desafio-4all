package br.com.dkauf.desafio_4all.ui.activity

import android.Manifest
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.dkauf.desafio_4all.R
import br.com.dkauf.desafio_4all.adapter.CommentsAdapter
import br.com.dkauf.desafio_4all.adapter.MainAdapter
import br.com.dkauf.desafio_4all.databinding.ActivityPrincipalBinding
import br.com.dkauf.desafio_4all.viewmodel.PrincipalViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import org.koin.android.viewmodel.ext.android.viewModel


class PrincipalActivity : AppCompatActivity(), OnMapReadyCallback {

    lateinit var binding: ActivityPrincipalBinding

    private val viewModel: PrincipalViewModel by viewModel()

    companion object {
        private const val PERMISSION_REQUEST_CODE = 790
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        intent.extras?.let { extra ->
            binding = DataBindingUtil.setContentView<ActivityPrincipalBinding>(
                this,
                R.layout.activity_principal
            )
                .also {
                    it.viewModel = viewModel
                    it.lifecycleOwner = this@PrincipalActivity
                }

            viewModel.getTaskDetails(extra.getString(MainAdapter.BUILD_EXTRA).orEmpty())

            supportActionBar?.apply {
                setDisplayHomeAsUpEnabled(true)
                setDisplayShowHomeEnabled(true)
            }

            val supportMapFragment: SupportMapFragment =
                (supportFragmentManager.findFragmentById(R.id.myMap) as SupportMapFragment?)!!
            supportMapFragment.getMapAsync(this@PrincipalActivity)

            configToolbar()
            initObservers()
        }
    }

    fun initObservers() {
        observersErrorMessage()
        observersAdapter()
        observersCallPhone()
        observersServiceCalled()
        observersAddressCalled()
        observersCommentsCalled()
    }

    private fun observersErrorMessage() {
        viewModel.onErrorMessage.observe(this, Observer {
            it?.run {
                Snackbar.make(binding.imageView, this, Snackbar.LENGTH_SHORT).show()
            }
        })
    }

    private fun configToolbar() {
        binding.toolbar.apply {
            setNavigationIcon(R.drawable.ic_menu_back_buttom)
            setSupportActionBar(this)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_home, menu)

        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchMenuItem = menu?.findItem(R.id.action_search)
        (searchMenuItem?.actionView as SearchView).apply {

            setSearchableInfo(searchManager.getSearchableInfo(componentName))
            setOnQueryTextListener(object : SearchView.OnQueryTextListener {

                override fun onQueryTextSubmit(query: String?): Boolean {
                    Log.e("Dkauf", "onQueryTextSubmit   ${query}")
                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    Log.e("Dkauf", "onQueryTextChange   ${newText}")
                    return true
                }

            })
        }

        return super.onCreateOptionsMenu(menu)
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        viewModel.response.observe(this, Observer {
            it?.apply {
                val latLng = LatLng(
                    latitude,
                    longitude
                )
                MarkerOptions().position(latLng).title(titulo).let {
                    googleMap?.apply {
                        animateCamera(CameraUpdateFactory.newLatLng(latLng))
                        animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17f))
                        addMarker(it)
                    }
                }
            }
        })
    }

    fun observersAdapter() {
        viewModel.response.observe(this, Observer {

            binding.recycler.apply {
                layoutManager =
                    LinearLayoutManager(this@PrincipalActivity)

                adapter = CommentsAdapter(
                    this@PrincipalActivity,
                    it.comentarios
                )
            }
        })
    }

    fun observersCallPhone() {
        viewModel.onCallPhoneCalled.observe(this, Observer {
            startCallPhone(it)
        })
    }

    fun observersServiceCalled() {
        viewModel.onServiceCalled.observe(this, Observer {
            startActivity(Intent(this, ServiceActivity::class.java))
        })
    }

    fun observersAddressCalled() {
        viewModel.onAddressCalled.observe(this, Observer {
            it?.run {
                Snackbar.make(binding.imageView, this, Snackbar.LENGTH_SHORT).show()
            }
        })
    }

    fun observersCommentsCalled() {
        viewModel.onCommentsCalled.observe(this, Observer {
            binding.appBarLayout.setExpanded(false)
            binding.nestedScroolView.fullScroll(binding.recycler.top)
        })
    }

    private fun startCallPhone(it: String?) {
        if (checkPermission(
                Manifest.permission.CALL_PHONE,
                PERMISSION_REQUEST_CODE
            )
        ) {
            val uri = Uri.parse("tel:$it")
            Intent(Intent.ACTION_DIAL, uri).apply {
                startActivity(this)
            }
        }
    }

    fun checkPermission(permission: String, requestCode: Int): Boolean {
        if (ContextCompat.checkSelfPermission(
                this@PrincipalActivity,
                permission
            ) == PackageManager.PERMISSION_DENIED
        ) {
            ActivityCompat.requestPermissions(
                this@PrincipalActivity,
                arrayOf(permission),
                requestCode
            )
            return false
        }
        return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startCallPhone(viewModel.response.value?.telefone)
            }
        }
    }

}