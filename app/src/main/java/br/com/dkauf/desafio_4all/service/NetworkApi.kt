package br.com.dkauf.desafio_4all.service

import br.com.dkauf.desafio_4all.model.TaskDetailsResponse
import br.com.dkauf.desafio_4all.model.TaskResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface NetworkApi {
    @GET("/tarefa")
    fun getTasks(): Single<TaskResponse>

    @GET("/tarefa/{id}")
    fun getTasksDetails(@Path("id") id: String): Single<TaskDetailsResponse>
}