package br.com.dkauf.desafio_4all.ui.custom

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.VisibleForTesting
import androidx.appcompat.view.ContextThemeWrapper
import androidx.constraintlayout.widget.ConstraintLayout
import br.com.dkauf.desafio_4all.R

open class ItemComponent @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(
    ContextThemeWrapper(
        context,
        null
    ), attrs, defStyleAttr
) {
    var description: TextView? = null
    var imageIcon: ImageView? = null

    init {
        inflateLayout()
        bindViews()
        initView(attrs)
    }

    private fun inflateLayout() {
        View.inflate(context, R.layout.layout_component, this)
    }

    private fun bindViews() {
        description = findViewById(R.id.textview)
        imageIcon = findViewById(R.id.imageView)
    }

    private fun initView(attrs: AttributeSet?) {
        attrs ?: return

        val attributeValues = context.obtainStyledAttributes(attrs, R.styleable.ItemComponent)
        with(attributeValues) {
            try {
                labelText(getString(R.styleable.ItemComponent_labelText))
                imageValue(getDrawable(R.styleable.ItemComponent_imageValue))
            } finally {
                recycle()
            }
        }
    }

    @VisibleForTesting
    fun imageValue(image: Drawable?) {
        image?.apply {
            imageIcon?.setImageDrawable(image)
        }
    }

    @VisibleForTesting
    fun labelText(text: String?) {
        description?.text = text
    }
}