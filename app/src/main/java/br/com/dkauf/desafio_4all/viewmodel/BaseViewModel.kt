package br.com.dkauf.desafio_4all.viewmodel

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class BaseViewModel: ViewModel() {

    var contentVisibility = MutableLiveData<Int>()
    var isLoadingDialog = MutableLiveData<Int>()
    var onErrorMessage = MutableLiveData<String>()

    init {
        contentVisibility.value = View.GONE
        isLoadingDialog.value = View.GONE
    }
}