package br.com.dkauf.desafio_4all.model

data class CommentsResponse(
    val urlFoto: String,
    val nome: String,
    val nota: Int,
    val titulo: String,
    val comentario: String
)