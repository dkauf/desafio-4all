package br.com.dkauf.desafio_4all.viewmodel

import android.app.Application
import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import br.com.dkauf.desafio_4all.R
import br.com.dkauf.desafio_4all.model.TaskDetailsResponse
import br.com.dkauf.desafio_4all.service.NetworkService
import br.com.dkauf.desafio_4all.utils.ConnectionUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class PrincipalViewModel(
    private val app: Application,
    private val repository: NetworkService
) : BaseViewModel() {

    var response = MutableLiveData<TaskDetailsResponse>()

    var onCallPhoneCalled = MutableLiveData<String>()
    var onServiceCalled = MutableLiveData<Boolean>()
    var onAddressCalled = MutableLiveData<String>()
    var onCommentsCalled = MutableLiveData<Boolean>()

    fun getTaskDetails(id:String) {
        takeIf {
            ConnectionUtil.hasInternet()
        }?.run {

            repository.getTasksDetails(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    contentVisibility.value = View.GONE
                    isLoadingDialog.value = View.VISIBLE
                }
                .doFinally {
                    isLoadingDialog.value = View.GONE
                }
                .subscribeBy(
                    onSuccess = {
                        response.value = it
                        contentVisibility.value = View.VISIBLE
                    },
                    onError = {
                        onErrorMessage.postValue(app.getString(R.string.generic_server_error_message))
                    }
                )
        } ?: onErrorMessage.postValue(app.getString(R.string.no_internet_error_message))
    }

    fun onClickCall() {
        onCallPhoneCalled.postValue(response.value?.telefone)
    }

    fun onClickService() {
        onServiceCalled.postValue(true)
    }

    fun onClickAddress() {
        onAddressCalled.postValue(response.value?.endereco)
    }

    fun onClickComments() {
        onCommentsCalled.postValue(true)
    }
}