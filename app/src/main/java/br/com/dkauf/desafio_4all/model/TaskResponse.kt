package br.com.dkauf.desafio_4all.model

import com.google.gson.annotations.SerializedName

data class TaskResponse(
    @SerializedName("lista")
    val taskList: List<String>)