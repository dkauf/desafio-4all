package br.com.dkauf.desafio_4all.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import br.com.dkauf.desafio_4all.R
import br.com.dkauf.desafio_4all.databinding.ContentCommentBinding
import br.com.dkauf.desafio_4all.model.CommentsResponse

class CommentsAdapter(
    val context: Context?,
    var listComments: List<CommentsResponse>?
) : RecyclerView.Adapter<CommentsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.content_comment,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return listComments?.size ?: 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        listComments?.apply {
            holder.setData(this[position])
        }
    }

    class ViewHolder(val binding: ContentCommentBinding) : RecyclerView.ViewHolder(binding.root) {
        fun setData(comment: CommentsResponse) {
            binding.comment =  comment
        }
    }
}