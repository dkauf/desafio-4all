package br.com.dkauf.desafio_4all.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.dkauf.desafio_4all.R
import br.com.dkauf.desafio_4all.adapter.MainAdapter
import br.com.dkauf.desafio_4all.databinding.ActivityMainBinding
import br.com.dkauf.desafio_4all.viewmodel.MainViewModel
import com.google.android.material.snackbar.Snackbar
import org.koin.android.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    private val viewModel: MainViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView<ActivityMainBinding>(
            this,
            R.layout.activity_main
        )
            .also {
                it.viewModel = viewModel
                it.lifecycleOwner = this@MainActivity
            }
        viewModel.getTasks()

        observersAdapter()
        observersErrorMessage()
    }

    fun observersAdapter() {
        viewModel.response.observe(this, Observer {

            binding.recycler.apply {
                layoutManager =
                    LinearLayoutManager(this@MainActivity)

                addItemDecoration(
                    DividerItemDecoration(
                        this@MainActivity,
                        DividerItemDecoration.VERTICAL
                    )
                )
                adapter = MainAdapter(
                    this@MainActivity,
                    it.taskList
                )
            }
        })
    }

    private fun observersErrorMessage() {
        viewModel.onErrorMessage.observe(this, Observer {
            it?.run {
                Snackbar.make(binding.title, this, Snackbar.LENGTH_SHORT).show()
            }
        })
    }
}
