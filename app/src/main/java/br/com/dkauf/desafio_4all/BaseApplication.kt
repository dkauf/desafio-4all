package br.com.dkauf.desafio_4all

import android.app.Application
import br.com.dkauf.desafio_4all.di.loadCreditKoinModules
import br.com.dkauf.desafio_4all.utils.ConnectionUtil
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class BaseApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        ConnectionUtil.init(this)

        startKoin {
            androidContext(this@BaseApplication)
            loadCreditKoinModules()
        }
    }
}