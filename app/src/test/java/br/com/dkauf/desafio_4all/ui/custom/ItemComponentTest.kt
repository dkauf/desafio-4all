package br.com.dkauf.desafio_4all.ui.custom

import android.app.Application
import androidx.test.core.app.ApplicationProvider
import br.com.dkauf.desafio_4all.R
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.nullValue
import org.hamcrest.MatcherAssert
import org.junit.After
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.junit.runner.RunWith
import org.koin.core.context.stopKoin
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.Shadows
import org.robolectric.Shadows.shadowOf
import org.robolectric.shadows.ShadowDrawable

@RunWith(RobolectricTestRunner::class)
class ItemComponentTest {

    private lateinit var sut: ItemComponent

    @Before
    fun setUp(){
        sut = ItemComponent(ApplicationProvider.getApplicationContext())
    }

    @After
    fun tearDown() {
        stopKoin()
    }

    @Test
    fun `set label text SUCCESS`() {
        sut.labelText("Title")

        MatcherAssert.assertThat(
            sut.description?.text.toString(),
            `is`("Title")
        )
    }

    @Test
    fun `set Label text NULL`() {
        sut.labelText(null)

        MatcherAssert.assertThat(
            sut.description?.text.toString(),
            `is`("")
        )
    }

    @Test
    fun `set Label NULL`() {
        sut.description = null

        assertThat(
            sut.description,
            `is`(nullValue())
        )
    }
}