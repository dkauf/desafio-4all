package br.com.dkauf.desafio_4all.di

import android.app.Application
import org.junit.After
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.test.KoinTest
import org.koin.test.check.checkModules
import org.mockito.Mockito.mock
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class KoinModuleTest: KoinTest {

    val mockedAndroidContext = mock(Application::class.java)

    @After
    fun tearDown() {
        stopKoin()
    }

    @Test
    fun testRetrofitModules() {
        stopKoin()
        startKoin {
            androidContext(mockedAndroidContext)
            modules(
                listOf(
                    servicesModule,
                    viewModelModule
                )
            )
        }.checkModules()
    }
}