package br.com.dkauf.desafio_4all.viewmodel

import android.app.Application
import android.view.View
import androidx.test.core.app.ApplicationProvider
import br.com.dkauf.desafio_4all.BaseApplicationTest
import br.com.dkauf.desafio_4all.R
import br.com.dkauf.desafio_4all.SynchronousTestSchedulerRule
import br.com.dkauf.desafio_4all.model.TaskDetailsResponse
import br.com.dkauf.desafio_4all.service.NetworkService
import br.com.dkauf.desafio_4all.utils.ConnectionUtil
import com.google.gson.Gson
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockkObject
import io.mockk.unmockkAll
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject
import junit.framework.Assert.assertEquals
import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.`is`
import org.junit.After
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.stopKoin
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class PrincipalViewModelTest : BaseApplicationTest() {

    @Rule
    @JvmField
    val testRule = SynchronousTestSchedulerRule()

    private lateinit var sut: PrincipalViewModel

    private val context: Application = ApplicationProvider.getApplicationContext()

    @MockK
    private lateinit var networkService: NetworkService

    private val delayer = PublishSubject.create<Boolean>()

    private val jsonResponse =
        "{\"id\":\"1\",\"cidade\":\"Porto Alegre\",\"bairro\":\"Moinhos de Vento\",\"urlFoto\":\"https://cdn.pixabay.com/photo/2016/06/02/02/33/triangles-1430105__340.png\",\"urlLogo\":\"https://gtswiki.gt-beginners.net/decal/png/32/04/70/6989670193304700432_1.png\",\"titulo\":\"Lorem Ipsum\",\"telefone\":\"5122333311\",\"texto\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus finibus consequat nulla, a laoreet ipsum blandit ac. Donec vitae convallis nisi. Mauris molestie id lorem quis dignissim. Cum sociis natoque penatibus et magnis dis parturient montes.\",\"endereco\":\"Avenida Carlos Gomes, 532\",\"latitude\":-30.0306551,\"longitude\":-51.1846846,\"comentarios\":[{\"urlFoto\":\"https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR90x_zKVj6dasLKz3UjmcFhovZUPaIWC0fq8VoecgKU12Bs_Hx\",\"nome\":\"Lorem Ipsum\",\"nota\":2,\"titulo\":\"Consequat Nulla\",\"comentario\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus finibus consequat nulla, a laoreet ipsum blandit ac. Donec vitae convallis nisi.\"},{\"urlFoto\":\"https://icons.iconarchive.com/icons/artua/dragon-soft/512/User-icon.png\",\"nome\":\"Mauris Molestie\",\"nota\":5,\"titulo\":\"finibus consequat\",\"comentario\":\"Aliquam eu lacinia justo. Aliquam a arcu nibh. Integer tincidunt nisi ac nibh posuere, in finibus turpis eleifend. Duis augue mauris, scelerisque non sem a, dictum volutpat urna.\"},{\"urlFoto\":\"https://icons.iconarchive.com/icons/artua/dragon-soft/512/User-icon.png\",\"nome\":\"Mauris Molestie\",\"nota\":5,\"titulo\":\"finibus consequat\",\"comentario\":\"Aliquam eu lacinia justo. Aliquam a arcu nibh. Integer tincidunt nisi ac nibh posuere, in finibus turpis eleifend. Duis augue mauris, scelerisque non sem a, dictum volutpat urna.\"},{\"urlFoto\":\"https://icons.iconarchive.com/icons/artua/dragon-soft/512/User-icon.png\",\"nome\":\"Mauris Molestie\",\"nota\":5,\"titulo\":\"finibus consequat\",\"comentario\":\"Aliquam eu lacinia justo. Aliquam a arcu nibh. Integer tincidunt nisi ac nibh posuere, in finibus turpis eleifend. Duis augue mauris, scelerisque non sem a, dictum volutpat urna.\"},{\"urlFoto\":\"https://icons.iconarchive.com/icons/artua/dragon-soft/512/User-icon.png\",\"nome\":\"Mauris Molestie\",\"nota\":5,\"titulo\":\"finibus consequat\",\"comentario\":\"Aliquam eu lacinia justo. Aliquam a arcu nibh. Integer tincidunt nisi ac nibh posuere, in finibus turpis eleifend. Duis augue mauris, scelerisque non sem a, dictum volutpat urna.\"},{\"urlFoto\":\"https://icons.iconarchive.com/icons/artua/dragon-soft/512/User-icon.png\",\"nome\":\"Mauris Molestie\",\"nota\":5,\"titulo\":\"finibus consequat\",\"comentario\":\"Aliquam eu lacinia justo. Aliquam a arcu nibh. Integer tincidunt nisi ac nibh posuere, in finibus turpis eleifend. Duis augue mauris, scelerisque non sem a, dictum volutpat urna.\"},{\"urlFoto\":\"https://icons.iconarchive.com/icons/artua/dragon-soft/512/User-icon.png\",\"nome\":\"Mauris Molestie\",\"nota\":5,\"titulo\":\"finibus consequat\",\"comentario\":\"Aliquam eu lacinia justo. Aliquam a arcu nibh. Integer tincidunt nisi ac nibh posuere, in finibus turpis eleifend. Duis augue mauris, scelerisque non sem a, dictum volutpat urna.\"}]}"

    private val response: TaskDetailsResponse =
        Gson().fromJson(jsonResponse, TaskDetailsResponse::class.java)

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        sut = PrincipalViewModel(context, networkService)
        sut.response.value = response
    }

    @After
    fun tearDown() {
        stopKoin()
    }

    @After
    fun afterTests() {
        unmockkAll()
    }

    @Test
    fun onClickCall() {
        sut.onClickCall()
        assertEquals(sut.onCallPhoneCalled.value, sut.response.value?.telefone)
    }

    @Test
    fun onClickService() {
        sut.onClickService()
        assertEquals(sut.onServiceCalled.value, true)
    }

    @Test
    fun onClickAddress() {
        sut.onClickAddress()
        assertEquals(sut.onAddressCalled.value, sut.response.value?.endereco)
    }

    @Test
    fun onClickComments() {
        sut.onClickComments()
        assertEquals(sut.onCommentsCalled.value, true)
    }

    @Test
    fun `get Tasks Details SUCCESS`() {
        ConnectionUtil.init(context)

        every {
            networkService.getTasksDetails(any())
        } returns Single.just(
            response
        ).delaySubscription(delayer)

        sut.getTaskDetails("123456")

        delayer.onComplete()

        assertThat(sut.contentVisibility.value, `is`(View.VISIBLE))
    }

    @Test
    fun `get Tasks Details ERROR`() {
        ConnectionUtil.init(context)

        every {
            networkService.getTasksDetails(any())
        } returns Single.error(Exception())

        sut.getTaskDetails("123456")

        assertThat(
            sut.onErrorMessage.value,
            CoreMatchers.`is`(context.getString(R.string.generic_server_error_message))
        )
    }

    @Test
    fun `get Tasks WHITHOUT INTERNET`() {

        mockkObject(ConnectionUtil)

        every {
            ConnectionUtil.hasInternet()
        } returns false

        sut.getTaskDetails("123456")

        assertThat(
            sut.onErrorMessage.value,
            `is`(context.getString(R.string.no_internet_error_message))
        )
    }
}