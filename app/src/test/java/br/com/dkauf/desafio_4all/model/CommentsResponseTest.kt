package br.com.dkauf.desafio_4all.model

import junit.framework.Assert.assertTrue
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.stopKoin
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class CommentsResponseTest {

    private lateinit var sut: CommentsResponse

    @After
    fun tearDown() {
        stopKoin()
    }

    @Before
    fun setUp() {
        sut = CommentsResponse(
            urlFoto = "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR90x_zKVj6dasLKz3UjmcFhovZUPaIWC0fq8VoecgKU12Bs_Hx",
            nome = "Lorem Ipsum",
            nota = 2,
            titulo = "Consequat Nulla",
            comentario = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus finibus consequat nulla, a laoreet ipsum blandit ac. Donec vitae convallis nisi."
        )
    }

    @Test
    fun `comment IS NOT EMPTY`() {
        assertTrue(sut.comentario.isNotEmpty())
    }

    @Test
    fun `urlFoto IS NOT EMPTY`() {
        assertTrue(sut.urlFoto.isNotEmpty())
    }

    @Test
    fun `nome IS NOT EMPTY`() {
        assertTrue(sut.nome.isNotEmpty())
    }

    @Test
    fun `nota IS NOT EMPTY`() {
        Assert.assertEquals(sut.nota, 2)
    }

    @Test
    fun `titulo IS NOT EMPTY`() {
        assertTrue(sut.titulo.isNotEmpty())
    }

    @Test
    fun `comentario IS NOT EMPTY`() {
        assertTrue(sut.comentario.isNotEmpty())
    }
}