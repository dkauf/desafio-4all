package br.com.dkauf.desafio_4all.model

import junit.framework.Assert.assertTrue
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.stopKoin
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class TaskResponseTest{

    private lateinit var sut: TaskResponse

    @After
    fun tearDown() {
        stopKoin()
    }

    @Before
    fun setUp(){
        sut = TaskResponse( taskList =listOf(
            "1",
            "b"
            ,"c33"
            ,"ultimo"))
    }

    @Test
    fun `list NOT NULL`(){
        assertTrue(sut.taskList.isNotEmpty())
    }
}