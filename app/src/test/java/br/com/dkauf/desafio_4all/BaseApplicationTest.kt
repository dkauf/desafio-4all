package br.com.dkauf.desafio_4all

import android.app.Application
import br.com.dkauf.desafio_4all.utils.ConnectionUtil

open class BaseApplicationTest : Application(){

    override fun onCreate() {
        super.onCreate()
        ConnectionUtil.init(this)
    }
}
