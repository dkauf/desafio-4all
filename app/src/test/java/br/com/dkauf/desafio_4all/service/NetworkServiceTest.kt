package br.com.dkauf.desafio_4all.service

import io.mockk.mockk
import io.mockk.verify
import org.junit.After
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.stopKoin
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class NetworkServiceTest {

    private val networkApiMock: NetworkApi = mockk(relaxed = true)
    private val sut = NetworkService(networkApiMock)

    @After
    fun tearDown() {
        stopKoin()
    }

    @Test
    fun getTasks() {
        sut.getTasks()

        verify {
            networkApiMock.getTasks()
        }
    }

    @Test
    fun getTasksDetails() {
        sut.getTasksDetails("123456")

        verify {
            networkApiMock.getTasksDetails("123456")
        }
    }
}