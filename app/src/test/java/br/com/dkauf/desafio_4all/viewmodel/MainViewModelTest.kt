package br.com.dkauf.desafio_4all.viewmodel

import android.app.Application
import android.view.View
import androidx.test.core.app.ApplicationProvider
import br.com.dkauf.desafio_4all.BaseApplicationTest
import br.com.dkauf.desafio_4all.R
import br.com.dkauf.desafio_4all.SynchronousTestSchedulerRule
import br.com.dkauf.desafio_4all.model.TaskResponse
import br.com.dkauf.desafio_4all.service.NetworkService
import br.com.dkauf.desafio_4all.utils.ConnectionUtil
import io.mockk.*
import io.mockk.impl.annotations.MockK
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject
import org.hamcrest.CoreMatchers
import org.junit.*
import org.junit.runner.RunWith
import org.koin.core.context.stopKoin
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class MainViewModelTest : BaseApplicationTest() {

    @Rule
    @JvmField
    val testRule = SynchronousTestSchedulerRule()

    private lateinit var sut: MainViewModel

    private val context: Application = ApplicationProvider.getApplicationContext()

    @MockK
    private lateinit var networkService: NetworkService

    private val delayer = PublishSubject.create<Boolean>()

    private val response = TaskResponse(
        taskList = listOf("1", "b", "c33", "ultimo")
    )

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        ConnectionUtil.init(context)
        sut = MainViewModel(context, networkService)
    }

    @After
    fun tearDown() {
        stopKoin()
    }

    @After
    fun afterTests() {
        unmockkAll()
    }

    @Test
    fun `get Tasks SUCCESS`() {
        every {
            networkService.getTasks()
        } returns Single.just(
            response
        ).delaySubscription(delayer)

        sut.getTasks()

        delayer.onComplete()

        Assert.assertThat(sut.contentVisibility.value, CoreMatchers.`is`(View.VISIBLE))

    }

    @Test
    fun `get Tasks ERROR`() {
        every {
            networkService.getTasks()
        } returns Single.error(Exception())

        sut.getTasks()

        Assert.assertThat(
            sut.onErrorMessage.value,
            CoreMatchers.`is`(context.getString(R.string.generic_server_error_message))
        )
    }

    @Test
    fun `get Tasks WHITHOUT INTERNET`() {

        mockkObject(ConnectionUtil)

        every {
            ConnectionUtil.hasInternet()
        } returns false

        sut.getTasks()

        Assert.assertThat(
            sut.onErrorMessage.value,
            CoreMatchers.`is`(context.getString(R.string.no_internet_error_message))
        )
    }
}
