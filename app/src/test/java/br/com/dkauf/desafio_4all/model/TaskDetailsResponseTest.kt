package br.com.dkauf.desafio_4all.model

import com.google.gson.Gson
import org.junit.After
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.stopKoin
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class TaskDetailsResponseTest {

    private val jsonResponse =
        "{\"id\":\"1\",\"cidade\":\"Porto Alegre\",\"bairro\":\"Moinhos de Vento\",\"urlFoto\":\"https://cdn.pixabay.com/photo/2016/06/02/02/33/triangles-1430105__340.png\",\"urlLogo\":\"https://gtswiki.gt-beginners.net/decal/png/32/04/70/6989670193304700432_1.png\",\"titulo\":\"Lorem Ipsum\",\"telefone\":\"5122333311\",\"texto\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus finibus consequat nulla, a laoreet ipsum blandit ac. Donec vitae convallis nisi. Mauris molestie id lorem quis dignissim. Cum sociis natoque penatibus et magnis dis parturient montes.\",\"endereco\":\"Avenida Carlos Gomes, 532\",\"latitude\":-30.0306551,\"longitude\":-51.1846846,\"comentarios\":[{\"urlFoto\":\"https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR90x_zKVj6dasLKz3UjmcFhovZUPaIWC0fq8VoecgKU12Bs_Hx\",\"nome\":\"Lorem Ipsum\",\"nota\":2,\"titulo\":\"Consequat Nulla\",\"comentario\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus finibus consequat nulla, a laoreet ipsum blandit ac. Donec vitae convallis nisi.\"},{\"urlFoto\":\"https://icons.iconarchive.com/icons/artua/dragon-soft/512/User-icon.png\",\"nome\":\"Mauris Molestie\",\"nota\":5,\"titulo\":\"finibus consequat\",\"comentario\":\"Aliquam eu lacinia justo. Aliquam a arcu nibh. Integer tincidunt nisi ac nibh posuere, in finibus turpis eleifend. Duis augue mauris, scelerisque non sem a, dictum volutpat urna.\"},{\"urlFoto\":\"https://icons.iconarchive.com/icons/artua/dragon-soft/512/User-icon.png\",\"nome\":\"Mauris Molestie\",\"nota\":5,\"titulo\":\"finibus consequat\",\"comentario\":\"Aliquam eu lacinia justo. Aliquam a arcu nibh. Integer tincidunt nisi ac nibh posuere, in finibus turpis eleifend. Duis augue mauris, scelerisque non sem a, dictum volutpat urna.\"},{\"urlFoto\":\"https://icons.iconarchive.com/icons/artua/dragon-soft/512/User-icon.png\",\"nome\":\"Mauris Molestie\",\"nota\":5,\"titulo\":\"finibus consequat\",\"comentario\":\"Aliquam eu lacinia justo. Aliquam a arcu nibh. Integer tincidunt nisi ac nibh posuere, in finibus turpis eleifend. Duis augue mauris, scelerisque non sem a, dictum volutpat urna.\"},{\"urlFoto\":\"https://icons.iconarchive.com/icons/artua/dragon-soft/512/User-icon.png\",\"nome\":\"Mauris Molestie\",\"nota\":5,\"titulo\":\"finibus consequat\",\"comentario\":\"Aliquam eu lacinia justo. Aliquam a arcu nibh. Integer tincidunt nisi ac nibh posuere, in finibus turpis eleifend. Duis augue mauris, scelerisque non sem a, dictum volutpat urna.\"},{\"urlFoto\":\"https://icons.iconarchive.com/icons/artua/dragon-soft/512/User-icon.png\",\"nome\":\"Mauris Molestie\",\"nota\":5,\"titulo\":\"finibus consequat\",\"comentario\":\"Aliquam eu lacinia justo. Aliquam a arcu nibh. Integer tincidunt nisi ac nibh posuere, in finibus turpis eleifend. Duis augue mauris, scelerisque non sem a, dictum volutpat urna.\"},{\"urlFoto\":\"https://icons.iconarchive.com/icons/artua/dragon-soft/512/User-icon.png\",\"nome\":\"Mauris Molestie\",\"nota\":5,\"titulo\":\"finibus consequat\",\"comentario\":\"Aliquam eu lacinia justo. Aliquam a arcu nibh. Integer tincidunt nisi ac nibh posuere, in finibus turpis eleifend. Duis augue mauris, scelerisque non sem a, dictum volutpat urna.\"}]}"

    private val response: TaskDetailsResponse =
        Gson().fromJson(jsonResponse, TaskDetailsResponse::class.java)

    @After
    fun tearDown() {
        stopKoin()
    }

    @Test
    fun getResponse() {
        assertNotNull(response)
    }

    @Test
    fun cityNeighborhoodFormatted() {
        assertEquals(response.cityNeighborhoodFormatted(), "Porto Alegre - Moinhos de Vento")
    }

    @Test
    fun getId() {
        assertFalse(response.id.isNullOrBlank())
    }

    @Test
    fun getCidade() {
        assertFalse(response.cidade.isNullOrBlank())
    }

    @Test
    fun getBairro() {
        assertFalse(response.bairro.isNullOrBlank())
    }

    @Test
    fun getUrlFoto() {
        assertFalse(response.urlFoto.isNullOrBlank())
    }

    @Test
    fun getUrlLogo() {
        assertFalse(response.urlLogo.isNullOrBlank())
    }

    @Test
    fun getTitulo() {
        assertFalse(response.titulo.isNullOrBlank())
    }

    @Test
    fun getTelefone() {
        assertFalse(response.telefone.isNullOrBlank())
    }

    @Test
    fun getTexto() {
        assertFalse(response.texto.isNullOrBlank())
    }

    @Test
    fun getEndereco() {
        assertFalse(response.endereco.isNullOrBlank())
    }

    @Test
    fun getLatitude() {
        assertEquals(response.latitude, -30.0306551, 0.0)
    }

    @Test
    fun getLongitude() {
        assertEquals(response.longitude, -51.1846846, 0.0)
    }

    @Test
    fun getComentarios() {
        assertFalse(response.comentarios.isNullOrEmpty())
    }
}