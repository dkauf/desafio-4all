#### Android Project
- min SDK 23
- Databinding
- MVVM
- Koin
- Retrifit2
- RXJava
- Mockk, Robolectric, junit, koin-test

#### Backend configuration
- Instalar o [Mockoon](https://mockoon.com/)
- clicar no menu superior `Import/Export` -> `Import from clipboard` e busca o arquivo **mockoon_backend.json** que se encontra na raiz do projeto
